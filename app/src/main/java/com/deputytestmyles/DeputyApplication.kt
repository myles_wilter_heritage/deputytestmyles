package com.deputytestmyles

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class DeputyApplication: Application() {
}
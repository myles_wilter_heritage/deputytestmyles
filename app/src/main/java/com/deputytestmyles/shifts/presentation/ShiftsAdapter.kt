package com.deputytestmyles.shifts.presentation

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.deputytestmyles.R
import com.deputytestmyles.shifts.domain.ShiftEntity
import com.squareup.picasso.Picasso

class ShiftsAdapter(
    private val resources: Resources
) : RecyclerView.Adapter<ShiftsAdapter.MyViewHolder>() {

    private val shiftEntities: MutableList<ShiftEntity> = mutableListOf()

    class MyViewHolder(
        private val view: View,
        val image: ImageView,
        val startTime: TextView,
        val endTime: TextView
    ) : RecyclerView.ViewHolder(view)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShiftsAdapter.MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.shifts_listitem, parent, false)
        val image = view.findViewById<ImageView>(R.id.image)
        val startTime = view.findViewById<TextView>(R.id.startTime)
        val endTime = view.findViewById<TextView>(R.id.endTime)

        return MyViewHolder(view, image, startTime, endTime)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val shiftEntity = shiftEntities[position]
        // TODO wrap picasso so down the line we can easily swap between image libraries
        Picasso.get().load(shiftEntity.image).into(holder.image)

        holder.startTime.text = resources.getString(R.string.start, shiftEntity.start)
        if (shiftEntity.end.isEmpty()) {
            holder.endTime.text = resources.getString(R.string.shift_still_running)
        } else {
            holder.endTime.text = resources.getString(R.string.end, shiftEntity.end)
        }
    }

    override fun getItemCount() = shiftEntities.size

    fun updateData(list: List<ShiftEntity>) {
        shiftEntities.clear()
        shiftEntities.addAll(list)
        notifyDataSetChanged()
    }
}
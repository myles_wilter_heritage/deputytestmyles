package com.deputytestmyles.shifts.presentation

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.deputytestmyles.R
import com.deputytestmyles.util.hasLocationPermission
import com.deputytestmyles.util.requestLocationIfNotGranted
import com.google.android.gms.location.LocationServices
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.shifts_fragment.*

@AndroidEntryPoint
class ShiftsFragment : Fragment() {

    companion object {
        fun newInstance() = ShiftsFragment()
    }

    private val viewModel: ShiftsViewModel by viewModels()
    private val shiftsAdapter by lazy {
        ShiftsAdapter(resources)
    }
    private val fusedLocationClient by lazy {
        LocationServices.getFusedLocationProviderClient(requireContext())
    }
    private var startMenuItem: MenuItem? = null
    private var endMenuItem: MenuItem? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.shifts_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        observeViewModel()
        requestLocationIfNotGranted()
        viewModel.loadShifts()
    }

    private fun initView() {
        setHasOptionsMenu(true)
        swiperefresh.setOnRefreshListener {
            viewModel.loadShifts()
        }
        recyclerView.apply {
            adapter = shiftsAdapter
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun observeViewModel() {
        viewModel.state.observe(viewLifecycleOwner, Observer {
            swiperefresh.isRefreshing = it.isLoading

            if (it.shiftsEntity != null) {
                shiftsAdapter.updateData(it.shiftsEntity.list)
            }

            val showEnd = it.shiftsEntity?.isOpenShift == true
            startMenuItem?.isVisible = !showEnd
            endMenuItem?.isVisible = showEnd

            // TODO handle error
            // TODO handle emptyState
        })
    }

    @SuppressLint("MissingPermission")
    private fun startShift() {
        if (hasLocationPermission()) {
            // Assuming last location is sufficient
            fusedLocationClient.lastLocation.addOnSuccessListener {
                viewModel.startShift(it.latitude.toString(), it.longitude.toString())
            }
        } else {
            // Assuming we should still let them toggle shifts without location
            viewModel.startShift("0.0", "0.0")
        }
    }

    @SuppressLint("MissingPermission")
    private fun endShift() {
        if (hasLocationPermission()) {
            fusedLocationClient.lastLocation.addOnSuccessListener {
                viewModel.endShift(it.latitude.toString(), it.longitude.toString())
            }
        } else {
            viewModel.endShift("0.0", "0.0")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.shifts_menu, menu)
        startMenuItem = menu.findItem(R.id.start_shift)
        endMenuItem = menu.findItem(R.id.end_shift)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.start_shift -> startShift()
            R.id.end_shift -> endShift()
        }
        return super.onOptionsItemSelected(item)
    }
}
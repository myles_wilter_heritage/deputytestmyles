package com.deputytestmyles.shifts.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.deputytestmyles.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ShiftsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.shifts_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, ShiftsFragment.newInstance())
                .commitNow()
        }
    }
}
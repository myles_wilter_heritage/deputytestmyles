package com.deputytestmyles.shifts.presentation

import android.content.Context
import android.text.format.DateFormat
import java.util.Date

fun Date.toMyFormat(): String {
    return android.text.format.DateFormat.format("yyyy-MM-dd hh:mm:ss a", this).toString()
}
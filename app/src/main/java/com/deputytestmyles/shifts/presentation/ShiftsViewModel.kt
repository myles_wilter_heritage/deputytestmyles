package com.deputytestmyles.shifts.presentation

import android.util.Log
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.deputytestmyles.shifts.domain.GetShiftsUseCase
import com.deputytestmyles.shifts.domain.ShiftsEntity
import com.deputytestmyles.shifts.domain.ToggleShiftUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ShiftsViewModel @ViewModelInject constructor(
    private val getShiftsUseCase: GetShiftsUseCase,
    private val toggleShiftUseCase: ToggleShiftUseCase,
    @Assisted private val savedStateHandle: SavedStateHandle
): ViewModel()  {

    val state: MutableLiveData<ShiftsViewModelState> = MutableLiveData(ShiftsViewModelState())

    fun loadShifts() {
        viewModelScope.launch(Dispatchers.IO) {
            state.postValue(
                state.value?.copy(
                    isLoading = true,
                    error = null
                )
            )

            try {
                val shifts = getShiftsUseCase.getShifts()
                state.postValue(
                    state.value?.copy(
                        isLoading = false,
                        shiftsEntity = shifts
                    )
                )
            } catch (error: Exception) {
                Log.e("LOAD_SHIFTS", error.message.orEmpty())

                state.postValue(
                    state.value?.copy(
                        isLoading = false,
                        error = error
                    )
                )
            }
        }
    }

    fun startShift(lat: String, lon: String) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                toggleShiftUseCase.startShift(lat, lon)
                loadShifts()
            } catch (error: Exception) {
                Log.e("START_SHIFT", error.message.orEmpty())
                state.postValue(state.value?.copy(error = error))
            }
        }
    }

    fun endShift(lat: String, lon: String) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                toggleShiftUseCase.endShift(lat, lon)
                loadShifts()
            } catch (error: Exception) {
                Log.e("END_SHIFT", error.message.orEmpty())
                state.postValue(state.value?.copy(error = error))
            }
        }
    }
}


data class ShiftsViewModelState(
    val isLoading: Boolean = true,
    val shiftsEntity: ShiftsEntity? = null,
    val error: Throwable? = null
)

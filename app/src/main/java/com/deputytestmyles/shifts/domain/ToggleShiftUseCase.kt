package com.deputytestmyles.shifts.domain

import com.deputytestmyles.network.ShiftParams
import com.deputytestmyles.shifts.data.ShiftsRepo
import com.deputytestmyles.shifts.data.ToggleShiftResponse
import java.util.Date
import javax.inject.Inject

class ToggleShiftUseCase @Inject constructor(
    private val shiftsRepo: ShiftsRepo
) {

    suspend fun startShift(lat: String, lon: String): String {
        return shiftsRepo.startShift(ShiftParams(Date(), lat, lon))
    }

    suspend fun endShift(lat: String, lon: String): String {
        return shiftsRepo.endShift(ShiftParams(Date(), lat, lon))
    }
}
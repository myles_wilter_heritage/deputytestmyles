package com.deputytestmyles.shifts.domain

import java.util.Date

data class ShiftsEntity(
    val isOpenShift: Boolean,
    val list: List<ShiftEntity>
)

data class ShiftEntity(
    val id: Int,
    val start: String,
    val end: String,
    val startLatitude: String,
    val endLatitude: String,
    val image: String
)
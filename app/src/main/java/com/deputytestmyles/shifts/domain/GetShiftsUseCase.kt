package com.deputytestmyles.shifts.domain

import com.deputytestmyles.shifts.data.ShiftsRepo
import javax.inject.Inject

class GetShiftsUseCase @Inject constructor(
    private val shiftsRepo: ShiftsRepo
) {

    suspend fun getShifts(): ShiftsEntity {
        return shiftsRepo.getShifts()
    }
}
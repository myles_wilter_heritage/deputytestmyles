package com.deputytestmyles.shifts.di

import com.deputytestmyles.shifts.data.ShiftDataStore
import com.deputytestmyles.shifts.data.ShiftRemoteDataStore
import com.deputytestmyles.shifts.data.ShiftsRepo
import com.deputytestmyles.shifts.data.ShiftsRepoImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@Module
@InstallIn(ActivityRetainedComponent::class)
abstract class ShiftsNetworkModule {

    @Binds
    abstract fun bindDataStore(remoteDataStore: ShiftRemoteDataStore): ShiftDataStore

    @Binds
    abstract fun bindRepo(remoteDataStore: ShiftsRepoImpl): ShiftsRepo
}
package com.deputytestmyles.shifts.data

import com.deputytestmyles.network.ShiftParams
import com.deputytestmyles.shifts.domain.ShiftsEntity

interface ShiftsRepo {
    suspend fun getShifts(): ShiftsEntity
    suspend fun startShift(shiftParams: ShiftParams): String
    suspend fun endShift(shiftParams: ShiftParams): String
}
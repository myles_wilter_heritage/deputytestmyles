package com.deputytestmyles.shifts.data

import com.deputytestmyles.network.ShiftParams
import com.deputytestmyles.shifts.domain.ShiftsEntity
import javax.inject.Inject

class ShiftsRepoImpl @Inject constructor(
    private val shiftDataStore: ShiftDataStore
): ShiftsRepo {

    override suspend fun getShifts(): ShiftsEntity {
        return shiftDataStore.getShifts()
    }

    override suspend fun startShift(shiftParams: ShiftParams): String {
        return shiftDataStore.startShift(shiftParams)
    }

    override suspend fun endShift(shiftParams: ShiftParams): String {
        return shiftDataStore.endShift(shiftParams)
    }
}
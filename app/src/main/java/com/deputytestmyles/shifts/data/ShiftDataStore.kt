package com.deputytestmyles.shifts.data

import com.deputytestmyles.network.ShiftParams
import com.deputytestmyles.shifts.domain.ShiftsEntity

interface ShiftDataStore {
    suspend fun getShifts(): ShiftsEntity
    suspend fun startShift(shiftParams: ShiftParams): String
    suspend fun endShift(shiftParams: ShiftParams): String
}
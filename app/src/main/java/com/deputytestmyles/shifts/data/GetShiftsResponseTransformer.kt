package com.deputytestmyles.shifts.data

import com.deputytestmyles.shifts.domain.ShiftEntity
import com.deputytestmyles.shifts.domain.ShiftsEntity
import javax.inject.Inject

class GetShiftsResponseTransformer @Inject constructor() {

    fun List<ShiftsResponse>?.toEntity(): ShiftsEntity {
        if (isNullOrEmpty()) {
            return ShiftsEntity(false, emptyList())
        }

        val isOpenShift = requireNotNull(this).last().end.isEmpty()
        val shifts  = requireNotNull(this).map {
            it.toEntity()
        }.asReversed()

        return ShiftsEntity(isOpenShift, shifts)
    }

    private fun ShiftsResponse.toEntity(): ShiftEntity {
        return ShiftEntity(
            id = id,
            start = start,
            end = end,
            startLatitude = startLatitude,
            endLatitude = endLatitude,
            image = image
        )
    }
}
package com.deputytestmyles.shifts.data

import com.deputytestmyles.network.DeputyApiService
import com.deputytestmyles.network.ShiftParams
import com.deputytestmyles.shifts.domain.ShiftsEntity
import javax.inject.Inject

class ShiftRemoteDataStore @Inject constructor(
    private val apiService: DeputyApiService,
    private val getShiftsResponseTransformer: GetShiftsResponseTransformer
): ShiftDataStore {

    override suspend fun getShifts(): ShiftsEntity {
        with (getShiftsResponseTransformer) {
            return apiService.getShifts().toEntity()
        }
    }

    override suspend fun startShift(shiftParams: ShiftParams): String {
        return apiService.startShift(shiftParams)
    }

    override suspend fun endShift(shiftParams: ShiftParams): String {
        return apiService.endShift(shiftParams)
    }
}
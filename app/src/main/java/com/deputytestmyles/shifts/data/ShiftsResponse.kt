package com.deputytestmyles.shifts.data

data class ShiftsResponse(
    val id: Int,
    val start: String,
    val end: String, // TODO can't use Date here without custom GSON parsing due to possible ""
    val startLatitude: String,
    val endLatitude: String,
    val image: String
)

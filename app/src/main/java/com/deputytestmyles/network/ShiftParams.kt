package com.deputytestmyles.network

import java.util.Date

data class ShiftParams(
    val time: Date,
    val latitude: String,
    val longitude: String
)
package com.deputytestmyles.network

import com.deputytestmyles.shifts.data.ShiftsResponse
import com.deputytestmyles.shifts.data.ToggleShiftResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

interface DeputyApiService {

    // TODO move auth header to interceptor
    @Headers("Authorization: Deputy $SHA1")
    @GET("shifts")
    suspend fun getShifts(): List<ShiftsResponse>?

    @Headers("Authorization: Deputy $SHA1")
    @POST("shift/start")
    suspend fun startShift(@Body params: ShiftParams): String

    @Headers("Authorization: Deputy $SHA1")
    @POST("shift/end")
    suspend fun endShift(@Body params: ShiftParams): String
}
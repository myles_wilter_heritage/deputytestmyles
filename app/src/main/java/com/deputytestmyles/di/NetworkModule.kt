package com.deputytestmyles.di

import com.deputytestmyles.network.DeputyApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Want this to be a singleton so
 * @InstallIn(ApplicationComponent::class)
 */
@Module
@InstallIn(ApplicationComponent::class)
object NetworkModule {

    @Provides
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    fun provideApiService(retrofit: Retrofit): DeputyApiService {
        return retrofit.create(DeputyApiService::class.java)
    }
}

private const val BASE_URL = "https://apjoqdqpi3.execute-api.us-west-2.amazonaws.com/dmc/"
package com.deputytestmyles

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateHandle
import com.deputytestmyles.shifts.domain.GetShiftsUseCase
import com.deputytestmyles.shifts.domain.ShiftsEntity
import com.deputytestmyles.shifts.domain.ToggleShiftUseCase
import com.deputytestmyles.shifts.presentation.ShiftsViewModel
import com.deputytestmyles.shifts.presentation.ShiftsViewModelState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.atLeastOnce
import org.mockito.Mockito.mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ShiftsViewModelTest {

    private val getShiftsUseCase = mock(GetShiftsUseCase::class.java)
    private val toggleShiftsUseCase = mock(ToggleShiftUseCase::class.java)
    private val savedStateHandle = mock(SavedStateHandle::class.java)

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()
    @Mock
    private lateinit var vmStateObserver: Observer<ShiftsViewModelState>

    val viewModel = ShiftsViewModel(
        getShiftsUseCase,
        toggleShiftsUseCase,
        savedStateHandle
    )

    @Before
    fun init() {

    }

    @Test
    fun testShiftsRefreshedAfterStartingShift() {
        testCoroutineRule.runBlockingTest {
            `when`(toggleShiftsUseCase.startShift(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn("")
            viewModel.startShift("","")
            verify(toggleShiftsUseCase, times(1)).startShift(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())
            verify(getShiftsUseCase, times(1)).getShifts()
        }
    }

    @Test
    fun testLoadingShiftsUpdateLiveData() {
        testCoroutineRule.runBlockingTest {
            val entity = ShiftsEntity(true, emptyList())
            `when`(getShiftsUseCase.getShifts()).thenReturn(entity)

            viewModel.state.observeForever(vmStateObserver)
            viewModel.loadShifts()
            verify(vmStateObserver, atLeastOnce()).onChanged(ShiftsViewModelState(false, entity, null))
            viewModel.state.removeObserver(vmStateObserver)
        }

    }
}
package com.deputytestmyles

import com.deputytestmyles.shifts.data.GetShiftsResponseTransformer
import com.deputytestmyles.shifts.data.ShiftsResponse
import org.junit.Test

class GetShiftsResponseTransformerTest {

    val transformer = GetShiftsResponseTransformer()

    @Test
    fun testNoEmptyShiftWhenResponseEmpty() {

        val list: List<ShiftsResponse> = emptyList()

        with(transformer) {
            val entity = list.toEntity()
            assert(!entity.isOpenShift)
        }
    }

    @Test
    fun testMappings() {
        // TODO
    }


}